var x = 0;
var y = 0;
function deviceMotionEvent(event) {
	x = event.accelerationIncludingGravity.x;
	y = event.accelerationIncludingGravity.y;
}
window.addEventListener("devicemotion", this.deviceMotionEvent, true);
	
BasicGame.Game = function (game) {
    this.game;		//	a reference to the currently running game
    this.add;		//	used to add sprites, text, groups, etc
    this.camera;	//	a reference to the game camera
    this.cache;		//	the game cache
    this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;		//	for preloading assets
    this.math;		//	lots of useful common math operations
    this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
    this.stage;		//	the game stage
    this.time;		//	the clock
    this.tweens;	//	the tween manager
    this.world;		//	the game world
    this.particles;	//	the particle manager
    this.physics;	//	the physics manager
    this.rnd;		//	the repeatable random number generator

    //	You can use any of these from any function within this State.

	this.fillMap = [];
	// this.fillMap[0] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
	// this.fillMap[1] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[2] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[3] = [1, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 1];
	// this.fillMap[4] = [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[5] = [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[6] = [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[7] = [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[8] = [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[9] = [1, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 1];
	// this.fillMap[10] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[11] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[12] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[13] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[14] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[15] = [1, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 1];
	// this.fillMap[16] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[17] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	// this.fillMap[18] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 1];
	// this.fillMap[19] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
	this.fillMap[0]  = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
	this.fillMap[1]  = [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1];
	this.fillMap[2]  = [1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 0, 1];
	this.fillMap[3]  = [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	this.fillMap[4]  = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1];
	this.fillMap[5]  = [1, 0, 0, 0, 0, 0, 1, 1, 0, 4, 1, 1];
	this.fillMap[6]  = [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1];
	this.fillMap[7]  = [1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 1];
	this.fillMap[8]  = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
	this.fillMap[9]  = [1, 0, 1, 0, 0, 0, 0, 4, 0, 0, 0, 1];
	this.fillMap[10] = [1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1];
	this.fillMap[11] = [1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1];
	this.fillMap[12] = [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1];
	this.fillMap[13] = [1, 0, 0, 0, 0, 0, 0, 4, 0, 0, 4, 1];
	this.fillMap[14] = [1, 0, 4, 0, 0, 0, 1, 1, 0, 0, 0, 1];
	this.fillMap[15] = [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1];
	this.fillMap[16] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1];
	this.fillMap[17] = [1, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 1];
	this.fillMap[18] = [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 3, 1];
	this.fillMap[19] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
	
	this.tileSize = 40;
	this.rows = 20;
	this.cols = 12;
	
	this.timer;
	this.max_speed = 500;
	this.acceler = 1000;
	this.drag = 750;
};

BasicGame.Game.prototype = {

	create: function () {
	
        var self = this;
		this.add.sprite(0, 0, 'background');
		
		this.grassGroup = this.add.group();
		this.boxGroup = this.add.group();
		this.finishGroup = this.add.group();
		this.blackholeGroup = this.add.group();
		this.playerGroup = this.add.group();
		for (i=0; i<this.rows; i++) {
			for (j=0; j<this.cols; j++) {
				var grass = this.add.sprite(j*this.tileSize, i*this.tileSize, "grass");
				this.grassGroup.add(grass);
				if(this.fillMap[i][j] == 1) {
					var box = this.add.sprite(j*this.tileSize, i*this.tileSize, "box");
					this.physics.enable(box, Phaser.Physics.ARCADE);
					box.enableBody = true;
					box.body.immovable = true;
					this.boxGroup.add(box);
				}
				if(this.fillMap[i][j] == 2) {
					var ball = this.add.sprite(j*this.tileSize, i*this.tileSize, "ball");
					this.physics.enable(ball, Phaser.Physics.ARCADE);
					ball.enableBody = true;
					ball.scale.set(0.75, 0.75);
					ball.body.bounce.x = 0.75; 
					ball.body.bounce.y = 0.75;
					ball.body.maxVelocity.setTo(this.max_speed, this.max_speed);
					ball.body.drag.setTo(this.drag, this.drag);
					this.playerGroup.add(ball);
					this.ball = ball;
				}
				if(this.fillMap[i][j] == 3) {
					var finish = this.add.sprite(j*this.tileSize, i*this.tileSize, "finish");
					finish.anchor.setTo(0.5, 0.5);
					this.physics.enable(finish, Phaser.Physics.ARCADE);
					finish.enableBody = true;
					finish.body.immovable = true;
					this.finishGroup.add(finish);
					this.finish = finish;
				}
				if(this.fillMap[i][j] == 4) {
					var blackhole = this.add.sprite(j*this.tileSize, i*this.tileSize, "blackhole");
					blackhole.anchor.setTo(0.5, 0.5);
					this.physics.enable(blackhole, Phaser.Physics.ARCADE);
					blackhole.enableBody = true;
					blackhole.body.immovable = true;
					this.blackholeGroup.add(blackhole);
					this.blackhole = blackhole;
				}
			}
		}
		this.cursors = this.input.keyboard.createCursorKeys();
		this.timer = Date.now();
	},

    pause: function() {
        if (!this.game.soundMute) {
            this.game.menuSelect.play();
        }

        this.game.add.tween(this.pauseButton.scale).
            to( { x: 1.1, y: 1.1 }, 150, Phaser.Easing.Linear.None, true, 0, 0, true);

        this.pauseboard.show();
    },

	update: function () {
		this.isFinish();
		this.inBlackhole();
		// this.physics.arcade.overlap(this.ball, this.boxGroup, this.collisionHandler, null, this);
		this.physics.arcade.collide(this.boxGroup, this.ball);
		
		this.finish.angle += 1.5;
		this.blackholeGroup.forEach(function(item) {
			item.angle -= 1.5;
		}, this);
		// this.ball.body.velocity.x = (-x)*25;
		this.ball.body.acceleration.x = (-x)*10;
		// this.ball.body.velocity.y = y*25;
		this.ball.body.acceleration.y = y*10;
		if (this.cursors.left.isDown) {
			// this.ball.body.velocity.x = -200;
			this.ball.body.acceleration.x = -this.acceler;
		}
		else if (this.cursors.right.isDown) {
			// this.ball.body.velocity.x = 200;
			this.ball.body.acceleration.x = this.acceler;
		}
		if (this.cursors.up.isDown) {
			// this.ball.body.velocity.y = -200;
			this.ball.body.acceleration.y = -this.acceler;
		}
		else if (this.cursors.down.isDown) {
			// this.ball.body.velocity.y = 200;
			this.ball.body.acceleration.y = this.acceler;
		}
	},

	quitGame: function (pointer) {
		this.state.start('MainMenu');
	},
	
	collisionHandler: function () {
		this.ball.kill();
	},
	
	inBlackhole: function () {
		this.blackholeGroup.forEach(function(item) {
			var tmp = 0;//item.width/20;
			if(this.ball.body.x > item.body.x + tmp
			&& this.ball.body.x + this.ball.width < item.body.x + item.body.width - tmp 
			&& this.ball.body.y > item.body.y + tmp
			&& this.ball.body.y + this.ball.height < item.body.y + item.body.height - tmp) {
				this.ball.kill();
				this.state.start('GameOver');
			}
		}, this);
	},
	
	isFinish: function () {
		var tmp = this.finish.width/10;
		if(this.ball.body.x > this.finish.body.x + tmp
		&& this.ball.body.x + this.ball.body.width < this.finish.body.x + this.finish.width - tmp 
		&& this.ball.body.y > this.finish.body.y + tmp
		&& this.ball.body.y + this.ball.body.height < this.finish.body.y + this.finish.height - tmp) {
			tween = this.game.add.tween(this.ball).to({ x: this.finish.x - (this.ball.width/2), y: this.finish.y - (this.ball.height/2) }, 1000, Phaser.Easing.Circular.None, true);
			var score = (Date.now() - this.timer)/1000;
			this.state.start('Scores', true, false, score);
		}
	}
};
