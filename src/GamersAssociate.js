BasicGame.GamersAssociate = function (game) {
};

BasicGame.GamersAssociate.prototype = {

	create: function () {
		var logo = this.add.sprite(this.game.width / 2, this.game.height / 2, 'logoPwr');
        logo.anchor.setTo(0.5, 0.5);
        var gaSound = this.add.audio('heartbeat');

        gaSound.play();
        logo.scale.setTo(0.5, 0.5);
        this.game.add.tween(logo.scale).to( { x: 0.8, y: 0.8 }, 150, Phaser.Easing.Linear.None, true, 0, 0, true);


        var self = this;
        setTimeout(function() {
                self.state.start('MainMenu');
            },
            2000);
	},

	update: function () {
	}

};
