var scores = [];
var url = "http://www.bartlomiej-zmudzinski.pl/mobile/game/score.php";
function load(addr) {
	$.ajax( {
		url: addr,
		type: 'GET',
		dataType: 'json',
		async: false,
		success: function(data) {
			for(var index in data) {
				scores.push(data[index]);
			}
		}
	});
}

BasicGame.Scores = function (game) {
	this.playButton = null;
	this.score;
};

BasicGame.Scores.prototype = {

	init: function(score) {
		scores = [];
		load(url + '?score=' + score);
		this.score = score;
		//scores.push(this.score);
	},

	create: function () {
        var self = this;
		
        this.background = this.add.sprite(0, 0, 'background');
		this.playButton = this.add.button(
			this.game.width/2,
			(this.game.height/10)*8,
			'play',
			this.startGame,
			this);
        this.playButton.anchor.setTo(0.5, 0.5);
		
		var timeLb = this.add.sprite(this.game.width/2, this.game.height/10, 'time').anchor.setTo(0.5, 0.5);
		var scoreTxt = this.add.text(this.game.width/2, 150, this.score, {
			font: 'bold 30pt Verdana',
			fill: '#00aa36',
			stroke: '#003E08',
			strokeThickness: 6}).anchor.setTo(0.5, 0.5);
		
		var bestLb = this.add.sprite(this.game.width/2, (this.game.height/10)*3, 'thebest');
		bestLb.enableBody = true;
		bestLb.scale.set(0.8, 0.8);
		bestLb.anchor.setTo(0.5, 0.5);
		
		var startLine = 300;
		var tab1 = 150;
		var tab2 = 350;
		if(scores.length > 0) {
			for(var i=0; i<Math.min(scores.length, 5); i++) {
				var color1 = "#fff";
				var color2 = "#000";
				if(scores[i] == this.score) {
					color1 = '#00aa36';
					color2 = '#003E08';
				}
				this.add.text(tab1, startLine, (i+1)+'.', {
					fill: color1,
					stroke: color2,
					strokeThickness: 6}).anchor.setTo(0, 0.5);
				this.add.text(tab2, startLine, scores[i], {
					fontWeight: 'bold',
					fontSize: 60,
					fill: color1,
					stroke: color2,
					strokeThickness: 6}).anchor.setTo(1, 0.5);
				startLine += 40;
			}
		}
	},

	update: function () {

	},

	startGame: function (pointer) {

        this.game.add.tween(this.playButton.scale).
            to( { x: 1.1, y: 1.1 }, 150, Phaser.Easing.Linear.None, true, 0, 0, true)
            .onComplete.add(this.startGameInternal, this);

		// this.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
		// this.scale.startFullScreen();
	},

    startGameInternal: function() {
        this.state.start('Game');//', true, false);
    }

};
