BasicGame.Preloader = function (game) {

	this.background = null;
	this.preloadBar = null;
	this.ready = false;

};

BasicGame.Preloader.prototype = {

	preload: function () {
		this.add.sprite(0, 0, 'background');
		
		var logoWidth = 460;
		this.add.sprite((this.game.width - logoWidth)/2, 20, 'title');

		this.game.paddingBot = 64;

		var barWidth = 250;
		var barHeight = 44;
		var barPaddingBot = 25
        var barX = (this.game.width - barWidth) / 2;
        var barY = this.game.height - barHeight - barPaddingBot - this.game.paddingBot;
        this.add.sprite(barX, barY, 'preloaderBarGray');
		this.preloadBar = this.add.sprite(barX, barY, 'preloaderBar');

		//	Tworzy automatyczny progressbar
		this.load.setPreloadSprite(this.preloadBar);
		
        this.load.image('logoPwr', 'images/pwr.png');
        this.load.audio('heartbeat', ['audio/heartbeat.mp3', 'audio/heartbeat.ogg']);

        this.load.image('play', 'images/control-play.png');
        this.load.audio('menuSelect', ['audio/menuselect.mp3', 'audio/menuselect.ogg']);
        this.load.image('pause', 'images/control-pause.png');
        this.load.image('home', 'images/control-home.png');
        this.load.image('restart', 'images/control-restart.png');
        // GAME
        this.load.image('gameover', 'images/gameover.png');
		this.load.image('time', 'images/time.png');
		this.load.image('thebest', 'images/thebest.png');
		
		this.load.image("box", "images/box40.png");
		this.load.image("grass", "images/grass40.png");
		this.load.image("ball", "images/ball40.png");
		this.load.image("finish", "images/finish80.png");
		this.load.image("blackhole", "images/blackhole80.png");
	},

	create: function () {
		this.preloadBar.cropEnabled = false;
	},

	update: function () {		
		if (this.ready == false)
		{
			this.ready = true;
			this.state.start('GamersAssociate');
		}
	}

};