BasicGame.MainMenu = function (game) {
	this.playButton = null;
};

BasicGame.MainMenu.prototype = {

	create: function () {
        var self = this;

        this.game.menuSelect = this.add.audio('menuSelect');
		
        this.background = this.add.sprite(0, 0, 'background');
		
		var logoWidth = 460;
		this.add.sprite((this.game.width - logoWidth)/2, 20, 'title');
		
		var playHeight = 200;
		var playPadding = 40;
		this.playButton = this.add.button(
			this.game.width/2,
			this.game.height - playHeight/2 - playPadding - this.game.paddingBot,
			'play',
			this.startGame,
			this);
        this.playButton.anchor.setTo(0.5, 0.5);
	},

	update: function () {

	},

	startGame: function (pointer) {
		this.game.menuSelect.play();

        this.game.add.tween(this.playButton.scale).
            to( { x: 1.1, y: 1.1 }, 150, Phaser.Easing.Linear.None, true, 0, 0, true)
            .onComplete.add(this.startGameInternal, this);

		// this.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
		// this.scale.startFullScreen();
	},

    startGameInternal: function() {
        this.state.start('Game');
    }

};
